<html>

<head>
  <title>Get Element By ID</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <link href='https://fonts.googleapis.com/css?family=Press+Start+2P' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="scorekeeper.css">
</head>

<body>
  <div class="container">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">SCORE KEEPER</h3>
      </div>
      <div class="panel-body">
        <h1><span id="p1Display">0</span> to <span id="p2Display">0</span></h1>
                <button id="p1">Player One</button>
        <button id="p2">Player Two</button>
        <hr><p>Playing to: <span>5</span></p>
        <input type="number" min="1">

        <button id="reset">Reset</button>
      </div>
    </div>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  <script type="text/javascript" src="scorekeeper.js"></script>
</body>

</html>

<style>
.container {  
  background-color: black;  
  border: 10px solid #4b2e83;  
  width: 500px; 
}

.panel {  
  margin: 20px auto;  
  width: 400px; 
}

.winner {  
  color: green; 
}

h1 span {  
  background-color: black;  
  font-family: 'Press Start 2P', cursive;  
  font-size: 60px;  
  color: red; 
}
</style>

<script>
var p1Button = document.querySelector("#p1"); 
var p2Button = document.getElementById("p2"); 
var resetButton = document.getElementById("reset"); 
var p1Display = document.querySelector("#p1Display"); 
var p2Display = document.querySelector("#p2Display"); 
var numInput = document.querySelector("input");

numInput.addEventListener('input', function(){
    var num = this.value.match(/^\d+$/);
    if (num === null) {
        this.value = "";
    }
}, false)

var winningScoreDisplay = document.querySelector("p span"); 
var p1Score = 0; 
var p2Score = 0; 
var gameOver = false; 
var winningScore = 5;

p1Button.classList.add('btn','btn-default');
p1Button.addEventListener("click", function() { 

  if (!gameOver) {     
    p1Score++; 
    if (p1Score === winningScore) { 
      p1Display.classList.add("winner");       
      gameOver = true;
      p1Button.classList.remove('btn','btn-default');
      p1Button.classList.add('btn','btn-success');
    }     
    p1Display.textContent = p1Score;   
  } 
});

p2Button.classList.add('btn','btn-default');
p2Button.addEventListener("click", function() {
  if (!gameOver) {     
    p2Score++;     
    if (p2Score === winningScore) {       
      p2Display.classList.add("winner"); 
      gameOver = true;     
      p2Button.classList.remove('btn','btn-default');
      p2Button.classList.add('btn','btn-success');
    }     
    
    p2Display.textContent = p2Score;   
  } 
});

resetButton.addEventListener("click", function() {   
  reset(); 
  p1Button.classList.remove('btn','btn-success');
  p1Button.classList.add('btn','btn-default');
  p2Button.classList.remove('btn','btn-success');
  p2Button.classList.add('btn','btn-default');
});

function reset() {   
  p1Score = 0;   
  p2Score = 0;   
  p1Display.textContent = 0; 
  p2Display.textContent = 0;
  p1Display.classList.remove("winner");
  p2Display.classList.remove("winner");  
  gameOver = false; 
}

numInput.addEventListener("change", function() {   winningScoreDisplay.textContent = this.value;
  winningScore = Number(this.value);
  reset(); 
});
</script>
